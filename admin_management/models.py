from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from core.models import BaseModel, Settings


class Inventory(BaseModel):
    item_name = models.CharField(max_length=100)
    quantity = models.FloatField(default=1)
    unit = models.CharField(max_length=5)
    buying_rate = models.DecimalField(default=0, max_digits=9, decimal_places=2)
    previous_buying_rate = models.DecimalField(default=0, max_digits=9, decimal_places=2, editable=False)

    def __str__(self):
        return self.item_name

    class Meta:
        ordering = ['item_name']
        verbose_name = 'Inventory'
        verbose_name_plural = 'Inventory'


class InventoryItemBuyingRate(BaseModel):
    item = models.ForeignKey(Inventory, on_delete=models.SET_NULL, null=True, related_name='buying_prices')
    amount = models.DecimalField(default=0, max_digits=9, decimal_places=2)
    set_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.item}: {self.amount}'

    class Meta:
        ordering = ['item__item_name',  'set_date']
        verbose_name = 'Item\'s previous buying rate'
        verbose_name_plural = 'Item\'s previous buying rates'


class Client(BaseModel):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = 'Client'
        verbose_name_plural = 'Clients'


class Stakeholder(BaseModel):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = 'Stakeholder'
        verbose_name_plural = 'Stakeholders'


class PriceList(BaseModel):
    item = models.ForeignKey(Inventory, on_delete=models.CASCADE)
    selling_rate = models.DecimalField(default=0, max_digits=9, decimal_places=2)
    previous_selling_rate = models.DecimalField(default=0, max_digits=9, decimal_places=2, editable=False)
    price_list_client = models.ForeignKey(Client, on_delete=models.CASCADE)

    def __str__(self):
        return self.price_list_client.name

    class Meta:
        ordering = ['price_list_client']
        verbose_name = 'Client Price Listing'
        verbose_name_plural = 'Client Price Listings'
        unique_together = ['price_list_client', 'item']


class ClientItemSellingRate(BaseModel):
    listing_item = models.ForeignKey(Inventory, on_delete=models.CASCADE, related_name='selling_prices')
    listing_client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='item_selling_prices')
    amount = models.DecimalField(default=0, max_digits=9, decimal_places=2)
    set_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.listing_client}-{self.listing_item}: {self.amount}'

    class Meta:
        ordering = ['listing_client__name', 'listing_item__item_name',  'set_date']
        verbose_name = 'Item\'s previous selling rate'
        verbose_name_plural = 'Item\'s previous selling rates'


class Order(BaseModel):
    class PaymentStatus(models.TextChoices):
        PENDING = 'PENDING', _('Pending')
        PAID = 'PAID', _('Paid')

    class FulfilmentStatus(models.TextChoices):
        FAILED = 'FAILED', _('Failed')
        PROCESSING = 'PROCESSING', _('Processing')
        DELIVERED = 'DELIVERED', _('Delivered')

    item = models.ForeignKey(Inventory, on_delete=models.SET_NULL, null=True)
    client = models.ForeignKey(Client, on_delete=models.SET_NULL, null=True)
    quantity = models.FloatField(default=0)
    delivery_date = models.DateField(default=timezone.now)
    fulfilled = models.CharField(max_length=10, choices=FulfilmentStatus.choices)
    payment_status = models.CharField(max_length=7, choices=PaymentStatus.choices)

    def clean(self):
        price_list_exists = self.client.pricelist_set.filter(item_id=self.item_id).exists()
        if not price_list_exists:
            raise ValidationError(f"The client \"{self.client}\" doesn't have a price listing for item \"{self.item}\".")

    def __str__(self):
        return f'{self.client} | {self.item} | {self.delivery_date}'

    class Meta:
        ordering = ['-delivery_date']
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.full_clean()
        super().save(force_insert, force_update, using, update_fields)


class Contribution(BaseModel):
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True)
    stakeholder = models.ForeignKey(Stakeholder, on_delete=models.SET_NULL, null=True)
    purchase_amount = models.DecimalField(default=0, max_digits=9, decimal_places=2)
    expenses = models.DecimalField(default=0, max_digits=9, decimal_places=2)

    def __str__(self):
        return str(self.order)

    class Meta:
        ordering = ['-created_at']
        verbose_name = 'Contribution'
        verbose_name_plural = 'Contributions'
        unique_together = ['order', 'stakeholder']
