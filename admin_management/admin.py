from django.contrib import admin

from admin_management.models import (Inventory, Client, PriceList, Contribution,
                                     Order, Stakeholder, InventoryItemBuyingRate, ClientItemSellingRate)


class InventoryAdmin(admin.ModelAdmin):
    list_display = ['item_name', 'quantity', 'unit', 'buying_rate']


class ClientAdmin(admin.ModelAdmin):
    list_display = ['name']


class PriceListAdmin(admin.ModelAdmin):
    list_display = ['price_list_client', 'item', 'selling_rate']


class StakeholderAdmin(admin.ModelAdmin):
    list_display = ['name']


class ContributionAdmin(admin.ModelAdmin):
    list_display = ['order', 'stakeholder', 'purchase_amount', 'expenses']


class ContributionInline(admin.TabularInline):
    model = Contribution
    extra = 1


class OrderAdmin(admin.ModelAdmin):
    list_display = ['item', 'client', 'quantity', 'delivery_date', 'payment_status', 'fulfilled']
    search_fields = ['client__name', 'item__item_name']
    list_filter = ['client__name', 'item__item_name', 'payment_status', 'fulfilled']
    inlines = [
        ContributionInline,
    ]


class InventoryItemBuyingRateAdmin(admin.ModelAdmin):
    list_display = ['item', 'amount', 'set_date', 'is_current_set']

    def is_current_set(self, obj):
        current_set_rate = InventoryItemBuyingRate.objects.filter(item_id=obj.item_id).order_by('-set_date').first()
        return current_set_rate.id == obj.id

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['item', 'amount', 'set_date', 'is_current_set']
        else:
            return []


class ClientItemSellingRateAdmin(admin.ModelAdmin):
    list_display = ['listing_client', 'listing_item', 'amount', 'set_date', 'is_current_set']

    def is_current_set(self, obj):
        current_set_rate = ClientItemSellingRate.objects.filter(listing_item_id=obj.listing_item_id,
                                                                listing_client_id=obj.listing_client_id) \
            .order_by('-set_date').first()
        return current_set_rate.id == obj.id

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['listing_client', 'listing_item', 'amount', 'set_date', 'is_current_set']
        else:
            return []


models = [
    (Inventory, InventoryAdmin),
    (Client, ClientAdmin),
    (PriceList, PriceListAdmin),
    (Contribution, ContributionAdmin),
    (InventoryItemBuyingRate, InventoryItemBuyingRateAdmin),
    (ClientItemSellingRate, ClientItemSellingRateAdmin),
    (Order, OrderAdmin),
    (Stakeholder, StakeholderAdmin),
]

for model in models:
    admin.site.register(model[0], model[1])
