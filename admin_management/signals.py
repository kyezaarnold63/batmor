from django.db.models.signals import post_save, post_init
from django.dispatch import receiver

from reporting_management.tasks import create_order_report
from admin_management.models import Order, Inventory, InventoryItemBuyingRate, PriceList, ClientItemSellingRate


@receiver(post_save, sender=Order)
def initialize_order_reporting_tasks(sender, instance, created, **kwargs):
    if created:
        create_order_report.delay(instance.id)


@receiver(post_init, sender=Inventory)
def remember_previous_buying_rate(sender, instance, **kwargs):
    instance.previous_buying_rate = instance.buying_rate


@receiver(post_save, sender=Inventory)
def create_buying_rate_record(sender, instance, created, **kwargs):
    pass
    if created:
        buying_rate = instance.buying_rate
        InventoryItemBuyingRate.objects.create(item_id=instance.id, amount=buying_rate)
    else:
        is_buying_rate_updated = instance.previous_buying_rate != instance.buying_rate
        if is_buying_rate_updated:
            instance.previous_buying_rate = instance.buying_rate
            instance.save()
            InventoryItemBuyingRate.objects.create(item_id=instance.id, amount=instance.buying_rate)


@receiver(post_init, sender=PriceList)
def remember_previous_selling_rate(sender, instance, **kwargs):
    instance.previous_selling_rate = instance.selling_rate


@receiver(post_save, sender=PriceList)
def create_selling_rate_record(sender, instance, created, **kwargs):
    pass
    if created:
        selling_rate = instance.selling_rate
        ClientItemSellingRate.objects.create(listing_item_id=instance.item.id,
                                             listing_client_id=instance.price_list_client.id,
                                             amount=selling_rate)
    else:
        is_selling_rate_updated = instance.previous_selling_rate != instance.selling_rate
        if is_selling_rate_updated:
            instance.previous_selling_rate = instance.selling_rate
            instance.save()
            ClientItemSellingRate.objects.create(listing_item_id=instance.item.id,
                                                 listing_client_id=instance.price_list_client.id,
                                                 amount=instance.selling_rate)
