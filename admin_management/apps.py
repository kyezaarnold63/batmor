from django.apps import AppConfig


class AdminManagementConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'admin_management'
    verbose_name = 'Management'

    def ready(self):
        from . import signals
