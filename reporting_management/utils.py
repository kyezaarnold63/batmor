from decimal import Decimal

from core.models import Settings


def total_amount(order):
    price_listing = order.client.pricelist_set.filter(item_id=order.item_id).first()
    return Decimal(order.quantity) * price_listing.selling_rate


def total_purchase_amount(order, created):
    contributors = order.contribution_set.all()
    from_contributors = sum([contributor.purchase_amount for contributor in contributors.iterator()])
    from_buying_price = Decimal(order.quantity) * order.item.buying_rate

    if from_buying_price == from_contributors:
        return from_buying_price
    elif from_contributors > from_buying_price and created:
        difference = from_contributors - from_buying_price
        contribution_for_each_stakeholder = difference / contributors.count()
        for contributor in contributors.iterator():
            current_expenses = contributor.expenses
            current_purchase_amount = contributor.purchase_amount
            contributor.expenses = current_expenses + contribution_for_each_stakeholder
            contributor.purchase_amount = current_purchase_amount - contribution_for_each_stakeholder
            contributor.save(update_fields=['expenses', 'purchase_amount'])
        return from_buying_price
    else:
        return Decimal(-1)


def total_expenses(order):
    contributors = order.contribution_set.all()
    return sum([contributor.expenses for contributor in contributors.iterator()])


def total_profit_and_marketeer_amount(order_total_amount, order_total_purchase_amount, order_total_expenses):
    config = Settings.load()
    marketeer_order_percentage_amount = Decimal(config.marketeer_order_percentage / float(100)) * order_total_amount
    total_profit_amount = ((order_total_amount - marketeer_order_percentage_amount) -
                           order_total_purchase_amount) - order_total_expenses
    return total_profit_amount, marketeer_order_percentage_amount


def get_contributor_total_profit(order, stakeholder_id):
    contribution = order.contribution_set.filter(stakeholder_id=stakeholder_id).first()
    if contribution:
        percentage_contribution = ((contribution.purchase_amount + contribution.expenses) /
                                   (order.order_report.order_cost + order.order_report.expenses))
        return percentage_contribution * order.order_report.profit
    return Decimal(0)


def get_contributor_investment(order, stakeholder_id):
    contribution = order.contribution_set.filter(stakeholder_id=stakeholder_id).first()
    return contribution.purchase_amount + contribution.expenses if contribution else Decimal(0)


def get_contributor_purchase_amount(order, stakeholder_id):
    contribution = order.contribution_set.filter(stakeholder_id=stakeholder_id).first()
    return contribution.purchase_amount if contribution else Decimal(0)


def get_contributor_expenses(order, stakeholder_id):
    contribution = order.contribution_set.filter(stakeholder_id=stakeholder_id).first()
    return contribution.expenses if contribution else Decimal(0)
