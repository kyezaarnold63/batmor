from django.urls import path

from reporting_management import views
from reporting_management.views import OrderReportListView,OrderReportDetailView

app_name = 'reporting_management'
urlpatterns = [
    path('', views.dashboard, name='dashboard'),
    path('reports/orders/general/', OrderReportListView.as_view(), name='order-report-list'),
    path('reports/orders/general/<int:pk>/', OrderReportDetailView.as_view(), name='order-report-detail'),
]
