from django.db import models
from django.utils import timezone

from core.models import BaseModel


class OrderReport(BaseModel):
    order = models.OneToOneField('admin_management.Order', on_delete=models.SET_NULL, null=True,
                                 related_name='order_report')
    item = models.CharField(max_length=100, null=True, blank=True)
    client = models.CharField(max_length=200, null=True, blank=True)
    quantity = models.FloatField(default=0, null=True, blank=True)
    unit = models.CharField(max_length=5, null=True, blank=True)
    delivery_date = models.DateField(default=timezone.now, null=True, blank=True)
    fulfilled = models.CharField(max_length=10, null=True, blank=True)
    payment_status = models.CharField(max_length=7, null=True, blank=True)
    item_buying_rate_record = models.ForeignKey('admin_management.InventoryItemBuyingRate',
                                                on_delete=models.SET_NULL, null=True, related_name='order_report')
    item_buying_rate = models.DecimalField(default=0, max_digits=16, decimal_places=2, null=True, blank=True)
    item_selling_rate_record = models.ForeignKey('admin_management.ClientItemSellingRate',
                                                 on_delete=models.SET_NULL, null=True, related_name='order_report')
    item_selling_rate = models.DecimalField(default=0, max_digits=16, decimal_places=2, null=True, blank=True)
    order_price = models.DecimalField(default=0, max_digits=16, decimal_places=2, null=True, blank=True)
    order_cost = models.DecimalField(default=0, max_digits=16, decimal_places=2, null=True, blank=True)
    expenses = models.DecimalField(default=0, max_digits=16, decimal_places=2, null=True, blank=True)
    marketeer_amount = models.DecimalField(default=0, max_digits=16, decimal_places=2, null=True, blank=True)
    order_investment_amount = models.DecimalField(default=0, max_digits=16, decimal_places=2, null=True, blank=True)
    profit = models.DecimalField(default=0, max_digits=16, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return f'{self.client} | {self.item} | {self.delivery_date}'

    class Meta:
        ordering = ['-delivery_date']
        verbose_name = 'Order report'
        verbose_name_plural = 'Order reports'


class OrderContributionReport(BaseModel):
    order = models.ForeignKey('admin_management.Order', on_delete=models.SET_NULL, null=True,
                              related_name='order_contributor_report')
    contributor = models.ForeignKey('admin_management.Stakeholder', on_delete=models.SET_NULL, null=True)
    contributor_order_cost = models.DecimalField(default=0, max_digits=16, decimal_places=2, null=True, blank=True)
    contributor_expenses = models.DecimalField(default=0, max_digits=16, decimal_places=2, null=True, blank=True)
    contributor_investment_amount = models.DecimalField(default=0, max_digits=9, decimal_places=2, null=True,
                                                        blank=True)
    contributor_profit = models.DecimalField(default=0, max_digits=16, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return f'{self.contributor.name}: {self.order}'

    class Meta:
        verbose_name = 'Contributor report'
        verbose_name_plural = 'Contributor reports'
        unique_together = ['order', 'contributor']
