import logging

from celery import shared_task

from reporting_management.models import OrderReport, OrderContributionReport
from reporting_management.utils import (total_amount, total_purchase_amount, total_expenses,
                                        total_profit_and_marketeer_amount,
                                        get_contributor_purchase_amount,
                                        get_contributor_expenses, get_contributor_investment,
                                        get_contributor_total_profit)

logger = logging.getLogger(__name__)


@shared_task
def create_order_report(order_id, **kwargs):
    obj, created = OrderReport.objects.get_or_create(
        order_id=order_id
    )
    order = obj.order
    if created:
        try:
            order_price = total_amount(order)
            order_cost = total_purchase_amount(order, created)
            expenses = total_expenses(order)
            buying_price = order.item.buying_prices.order_by('-set_date').first()
            selling_price = order.item.selling_prices.filter(listing_client_id=order.client_id).order_by(
                '-set_date').first()

            obj.item = order.item.item_name
            obj.client = order.client.name
            obj.quantity = order.quantity
            obj.unit = order.item.unit
            obj.delivery_date = order.delivery_date
            obj.fulfilled = order.FulfilmentStatus(order.fulfilled).label
            obj.payment_status = order.PaymentStatus(order.payment_status).label
            obj.item_buying_rate_record = buying_price
            obj.item_buying_rate = buying_price.amount
            obj.item_selling_rate_record = selling_price
            obj.item_selling_rate = selling_price.amount
            obj.order_price = order_price
            obj.order_cost = order_cost
            obj.expenses = expenses
            obj.order_investment_amount = order_cost + expenses
            profit, marketeer_amount = total_profit_and_marketeer_amount(order_price, order_cost, expenses)
            obj.profit = profit
            obj.marketeer_amount = marketeer_amount
            obj.save()
        except Exception as e:
            logger.error(f'Error creating order report for order {order_id} with client {str(order.client)} and item '
                         f'{str(order.item)}: {e}')
            obj.delete()
            return

        for contributor in order.contribution_set.iterator():
            create_contributor_report.delay(order_id, contributor.stakeholder_id)


@shared_task
def create_contributor_report(order_id, contributor_id, **kwargs):
    obj, created = OrderContributionReport.objects.get_or_create(
        order_id=order_id,
        contributor_id=contributor_id
    )
    order = obj.order
    if created:
        try:
            obj.contributor_id = contributor_id
            obj.contributor_order_cost = get_contributor_purchase_amount(order, contributor_id)
            obj.contributor_expenses = get_contributor_expenses(order, contributor_id)
            obj.contributor_investment_amount = get_contributor_investment(order, contributor_id)
            obj.contributor_profit = get_contributor_total_profit(order, contributor_id)
            obj.save()
        except Exception as e:
            logger.error(f'Error creating contributor report for order {order_id} and contributor {contributor_id}: {e}')
            obj.delete()
            return
