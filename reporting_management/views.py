from django.http import Http404
from django.shortcuts import render
from django.views.generic import DetailView, ListView

from reporting_management.models import OrderReport, OrderContributionReport


def dashboard(request):
    report = OrderContributionReport.objects.filter(contributor_id=2).first()
    context = {
        'report': report
    }
    return render(request, 'reporting_management/dashboard.html', context)


class OrderReportListView(ListView):
    model = OrderReport


class OrderReportDetailView(DetailView):
    model = OrderReport


# def order_report(request):
#     context = {
#         'orders': OrderReport.objects.all()
#     }
#     return render(request, 'reporting_management/orderreport_list.html', context)
