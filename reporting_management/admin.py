from django.contrib import admin

from reporting_management.models import OrderReport, OrderContributionReport


class OrderReportAdmin(admin.ModelAdmin):
    list_display = ['item', 'client', 'quantity', 'unit', 'delivery_date', 'fulfilled', 'payment_status',
                    'item_buying_rate', 'item_selling_rate', 'expected_income', 'order_cost', 'expenses',
                    'total_contribution', 'marketeer_amount', 'profit']

    def expected_income(self, obj):
        return obj.order_price

    def total_contribution(self, obj):
        return obj.order_investment_amount

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['item', 'client', 'quantity', 'unit', 'delivery_date', 'fulfilled', 'payment_status',
                    'item_buying_rate', 'item_selling_rate', 'expected_income', 'order_cost', 'expenses',
                    'total_contribution', 'marketeer_amount', 'profit']
        else:
            return []


class OrderContributionReportAdmin(admin.ModelAdmin):
    list_display = ['contributor', 'contributor_order_cost', 'contributor_expenses', 'contributor_investment_amount',
                    'contributor_profit']

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['contributor', 'contributor_order_cost', 'contributor_expenses', 'contributor_investment_amount',
                    'contributor_profit']
        else:
            return []


models = [
    (OrderReport, OrderReportAdmin),
    (OrderContributionReport, OrderContributionReportAdmin)
]

for model in models:
    admin.site.register(model[0], model[1])
