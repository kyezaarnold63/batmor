from django import template

register = template.Library()


@register.filter
def round_dec(amount):
    return round(amount, 0)
